import sqlalchemy
import datetime
import os.path

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy	import (
	Column,
	Integer,
	String,
	Float,
	DateTime,
	Time,
	Boolean,
	create_engine,
	ForeignKey
)

from sqlalchemy.orm import relationship, backref, sessionmaker

# engine = create_engine('sqlite:///foo.db')
DATABASE = 'sqlite:///db.sqlite3'
DEBUG = True
Base = declarative_base()

class UserPreferences(Base):
	__tablename__ = 'user_preference'

	id = Column(Integer, primary_key=True, autoincrement=True)
	user_batch_name = Column(String(20))
	desired_moisture = Column(Float)
	type_of_cacao = Column(String(15))
	date_created = Column(DateTime, default=datetime.datetime.now())

class BatchData(Base):
	__tablename__ = 'batch_data'

	id = Column(Integer, primary_key=True, autoincrement=True)
	batch_datetime = Column(DateTime, default=datetime.datetime.now())
	batch_type_of_cacao = Column(String(15))
	batch_desired_moisture_content = Column(Float)
	batch_name = Column(String(20))

class EnvVars(Base):
	__tablename__ = 'environment_vars'

	id = Column(Integer, primary_key=True, autoincrement=True)
	batch_info = Column(Integer, ForeignKey('batch_data.id'))
	initial_data = Column(Boolean, default = False)
	temperature = Column(Float)
	#Commented out for Simulation Purposes
	# humidity = Column(Float)
	dry_weight = Column(Float)
	read_time = Column(Time, default=datetime.datetime.now().time())
	graphed = Column(Boolean, default=False)

class FinalData(Base):
	__tablename__ = 'comps_data'

	id = Column(Integer, primary_key=True, autoincrement=True)
	batch_result = Column(Integer, ForeignKey('batch_data.id'))
	drying_rate = Column(Float)
	date_ended = Column(DateTime)
	final_moisture = Column(Float)
	last_weight = Column(Float)

engine = create_engine(DATABASE, echo=DEBUG)
session_factory = sessionmaker(bind=engine)
session = session_factory()

if not os.path.exists('db.sqlite3'):
	Base.metadata.create_all(engine)