import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
# GPIO.setup(16, GPIO.OUT)

class DCMotor(object):
	# GPIO.setup(16, GPIO.OUT)
	def __init__(self,pin):
		GPIO.setup(pin, GPIO.OUT)

	def sleeper(self, count):
		'''
		used to count how long the motor will be on in seconds
		'''
		time.sleep(count)

	def turnOn(self, pin):
		'''
		used to turn on the motor
		'''
		GPIO.output(pin, GPIO.HIGH)

		return "motor on"

	def turnOff(self, pin):
		'''
		used to turn off the motor
		'''
		GPIO.output(pin, GPIO.LOW)

		return "motor off"

	@staticmethod
	def cleaner(self):
		self.sleeper()
		GPIO.cleanup()

# try:
# 	# cls = DCMotor()
# 	while True:
# 		n = raw_input("[ON = O]\n[OFF = P]: ").lower()
# 		cls = DCMotor(16)
# 		txt = n.strip()
# 		if txt == 'o':
# 			# DCMotor().turnOn(DCMotor(), 16)
# 			cls.turnOn(cls, 16)
# 		elif txt == 'p':
# 			cls.turnOff(cls, 16)
# 		else:
# 			cls.cleaner(cls)
# 			break

# except KeyboardInterrupt:
# 	GPIO.cleanup()
# finally:
# 	GPIO.cleanup()